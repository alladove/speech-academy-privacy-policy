# User Provided Information
The application does not obtain or collect user's information.
The data required for normal operation of the application (such as speeches and app preferences entered by the user) is stored locally on user's device and is not transferred to us.

# Third Parties
We may disclose Automatically Collected Information as required by law, or to advertisers and analytics companies as described in the section below.

# Automatic Data Collection and Advertising 
We may work with analytics companies to help us understand how the application is being used, such as the frequency and duration of usage.
Additionally, we work with advertisers and third party advertising networks, who need to know how you interact with advertising provided in the application which helps us offer the application for free.
Advertisers and advertising networks use some of the information collected by the application, including, but not limited to, the unique identification ID of your mobile device and your mobile telephone number.
These third parties may also obtain anonymous information about other applications you’ve downloaded to your mobile device, the mobile websites you visit, your non-precise location information (e.g., your zip code), and other non- precise location information in order to help analyze and serve anonymous targeted advertising on the application and elsewhere.

# Children
We do not use the application to knowingly solicit data from or market to children under the age of 13.

# Your Consent
By using the application, you are consenting to our processing of your information as set forth in this Privacy Policy now and as amended by us. 
"Processing,” means using cookies on a computer/hand held device or using or touching information in any way, including, but not limited to, collecting, storing, deleting, using, combining and disclosing information, all of which activities will take place in the United States. 
If you reside outside the United States your information will be transferred, processed and stored there under United States privacy standards. 

# Contact us
If you have any questions regarding privacy while using the application, or have questions about our practices, please contact us via the PlayStore contact.